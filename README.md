# CS 351 - Programming Languages

This project contains materials for CS 351 - Programming Languages.
The project is configured for use with GitPod, which provides
a cloud-based development environment suitable for completing assignments.

Each student needs a GitLab and a GitPod account. The free tier of these
services should be sufficient to complete the assignments. Of course if
a student uses GitPod for other courses or work, then they may exceed the
number of free-teir credits. At the time of writing it is posible to
get 50 hours per month for free by linking a LinkedIn account (otherwise
you get 10 hours per month for free). Each additional 50 hours per month
costs $9. So over this 15 week course, I would expect most to spend nothing,
and in the extreme case 3 * $9 for a total of $27.

## Setup

1. Create a GitLab account.
2. Create a GitPod account.

Your instructor will ask you to provide them with your GitLab account
name so that they can give you access to your private copy of this account.

## Workflow

1. On GitLab, navigate to your private copy of this repository.
2. In your browser's location bar, ***prepend*** to the current URL
   "https://gitpod.io/#". That means your project's URL will come after
   the `#`.
3. Press enter.
4. The first time you do this, you will be asked to grant GitPod permissions
    over your GitLab account. Do so.
5. Accept the default configuration for a GitPod workspace.

You should now be looking at a VSCode environment in GitPod opened to the
root of your project. Within this environment you can create/delete/modify
files and run the PLCC command-line tools from within a terminal.

When you complete a work session and want to save your work,
you need to commit and push your work by running the following command
in your terminal:

```bash
$GITPOD_REPO_ROOT/bin/synch.bash "a short commit message"
```

Alternatively, from the root of the project...

```bash
git stage . && git commit -m "a short commit message" && git pull --rebase && git push
```

This command does the following:

1. It stages and commits all of your changes under the `assignments/` directory.
2. It pulls any new changes in your repository on GitLab (this may happen
   if your instructor pushes changes while you have been working in GitPod)
3. It pushes your work to your repository on GitLab.

> **Only commit changes in `assignments/`**
>
> Notice the command above only commit change you made to files in
> `assignments/`. Unless told to do otherwise, you only commit changes
> to files under this directory. This will help avoid git conflicts
> between you and your instructor.

Last, open your dashboard on GitPod and delete your workspace. An easy
way to get to your dashboard if you are still in a GitPod-VSCode session
is to press CTRL+P , search for and select "Open Dashboard". To delete
the workspace, click the `...` menu and select delete.

> **Delete my workspace?!**
>
> Yes! In GitPod, workspaces are indended to be effemeral. You create them
> when you need to make a (small) change. Commit and push your work. And
> then throw away the workspace.

## GitPod - Under the Hood

When you create and start a GitPod workspace for a project, GitPod
roughly does the following:

1. Creates and starts a Docker container.
2. Clones the project into that container.
3. Runs VS Code in that container.
4. Gives you access to that VS Code session through a browser.

Think of the workspace as a machine in the cloud on which you can work
on this project. The beauty of this is that you can work on a project
from any browser, and you don't need a powerful machine to do it! You
can even build and run docker Containers inside the workspace!!!
