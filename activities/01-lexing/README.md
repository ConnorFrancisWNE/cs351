# Lexing in PLCC

The goal of this activity are:

* Ensure you have a working development environment for PLCC.
* Use PLCC to build scanners from a lexical specification in a grammar file.
* Ensure you can access your individual homework repository on GitLab
    and are able to submit work to it.

## Setup

1. Navigate to your personal repository for this course on GitLab.
2. Open your repository in GitPod by prefixing the repository URL with
    `https://gitpod.io/#`, so your repository URL follows the `#` in the
    full URL. Hit enter so the browser opens it.
3. Create the workspace with the default configuration. This should open
    a VS Code in your browser attached to a clone of your repository.
4. Test by entering the following in VS Code's terminal:

    ```bash
    plcc --version
    ```

    You should see the version number of plcc, and not an error.

## Compiling and running a scanner interactively

`words.grammar` contains a lexical specification for a language.
Open it and familiarize yourself with it.

Let's compile and run the scanner.

1. Open a terminal in your development environment (in VS Code, CTRL+SHIFT+`).
2. Position your command-line in the same directory as `words.grammar`, using
   cd (do not type the $ in the examples. These represent the command prompt
   and are not entered by the user.)
    ```
    $ cd activities/01-lexing
    ```
3. Compile the grammar.
    ```
    $ plccmk words.grammar
    ```
4. Run the scanner. The scanner reads from standard input, which is the
    keyboard by default. So after you start the scanner, type some words
    on two or three lines and observe the scanner's output. Each time you
    press enter, the scanner scans the line and prints the tokens it
    recognizes.
    ```
    $ scan
    type some words
        1: WORD 'type'
        1: WORD 'some'
        1: WORD 'words'
    more word
        2: WORD 'more'
        2: WORD 'word'
    ```
    When you have had enough, press CTRL+D to send an EOF (end of file)
    character, or press CTRL+C to send a terminate signal.

Repeat the above, but this time enter some characters that the scanner
will not recognize and see what it does.

## Running a scanner on a file

More often we want to run our scanner on the contents of a file.
Let's run our scanner on `program1.words`. This file represents a program
written in our new `words` language. We have already built the scanner
in the last section. So we just need to run our scanner and redirect
standard input to the contents of `program1.words`. We do this using
`<` followed by the file name.

```
scan < program1.words
```

Try the last command to see that it works.

Write `program2.words`. Put some invalid "tokens" in it and scan it.

## Glance Under the Hood

Take a look at the files in `Java/`.
Open `Token.java` and find the definitions of the tokens from `words.grammar`.

`Scan.java` is the scanner. This is what you have been interacting with
when you ran `scan`. In fact, `scan` is a script that runs the compiled
`Scan.class` as follows

```
java -cp Java Scan
```

Try the above command and confirm that it behaves just as `scan` did.

## Exercise

Add a NUMBER token to words.grammar that matches non-negative integers like

```
0
56
234
```

Recompile the grammar. It's always a good idea to delete the Java folder
before you do.

```
$ rm -rm Java
```

This ensures that you always get the latest versions of all the Java and
class files.

Now run your new scanner on `program1.words` to ensure it still works,
and that `program2.words` still fails.

Now create `program3.words` that uses your new token, and make sure it works.

## Save your work to GitLab

First make sure your command-line is positioned in the root of your project.
Check what directory you are currently in

```bash
pwd
```

Assuming you are in `.../activities/01-lexing`, move two directories up as
follows (adjust the following based on your location; the goal is to be
in the directory that contains the `activities/` directory).

```bash
cd ../../
```

Then execute the following line.

```bash
git stage . && git commit -m "done: activities/01-lexing" && git pull --rebase && git push
```

This long one-line command will stage and commit your work to your local
repository. Then it pulls any new changes from origin (incase you or your
instructor made changes there), and then pushes the new local changes to
your origin repository.
