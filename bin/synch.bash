#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}/.."

if [ -z "$1" ] ; then
    >&2 echo "Please provide brief (<= 65 char) commit message."
    >&2 echo "usage: $0 \"MESSAGE\""
    exit 0
fi

git stage . && git commit -m "$1"
git pull --rebase && git push

if [ $? -ne 0 ] ; then
    >&2 echo ""
    >&2 echo "There was a problem synchronizing your GitPod workspace with"
    >&2 echo "your GitLab repository. Please read the error message(s) above."
    >&2 echo "If you are not sure how to correct the problem, do the following:"
    >&2 echo "  1. Take a screenshot of the error and the command(s) you think caused it."
    >&2 echo "  2. Stop the GitPod workspace, but don't delete it."
    >&2 echo "  3. Contact your instructor through Discord, giving them the screenshot."
    exit 1
fi
